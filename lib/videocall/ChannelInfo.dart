import 'dart:math';
import 'package:pano_rtc/pano_rtc.dart';

class ChannelInfo {
  static String appId = 'ad35a53c4c814b5d983d18da0ba54442';
  static String server = 'api.pano.video';
  static String token = '';
  static String channelId = '';
  static ChannelMode channelMode = ChannelMode.Meeting;
  static String userId = Random().nextInt(10000).toString();
  static String userName = '';

  static void setUserId(String userId) {
    ChannelInfo.userId = userId;
  }

  static void setChannelId(String channelId) {
    ChannelInfo.channelId = channelId;
  }

  static void setChannelMode(ChannelMode channelMode) {
    ChannelInfo.channelMode = channelMode;
  }

  static void setUserName(String userName) {
    ChannelInfo.userName = userName;
  }

  static void setToken(String token) {
    ChannelInfo.token = token;
  }
}
